package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
	public static void main( String args [ ] ) {
		
		
	}	

	public static boolean isValidFormat(String email) {
		if( email ==null)
		{
			return false;
		}
	      String line = email;
	      String pattern = "[\\w-]+@([\\w-]+\\.)+[\\w-]+";

	      Pattern r = Pattern.compile(pattern);

	      Matcher m = r.matcher(line);
	      if (m.find( ) ) {
	         return true;
	      }

		return false;
	}
	
	public static boolean isValidAccountName(String email) {
		if( email ==null)
		{
			return false;
		}
		String line = email;
	      String pattern = "^([^0-9][a-z]).{3,}@([\\w-]+\\.)+[\\w-]+";

	      Pattern r = Pattern.compile(pattern);

	      Matcher m = r.matcher(line);
	      if (m.find( ) ) {
	         return true;
	      }

		return false;
	}
	
	
	
	public static boolean isValidDomainName(String email) {
		if( email ==null)
		{
			return false;
		}
	      String line = email;
	      String pattern = "[\\w-]+@([a-z0-9]{2,}.+\\.)+[\\w-]+";

	      Pattern r = Pattern.compile(pattern);

	      Matcher m = r.matcher(line);
	      if (m.find( ) ) {
	         return true;
	      }

		return false;
	}
	
	public static boolean isValidExtensionName(String email) {
		if( email ==null)
		{
			return false;
		}
	      String line = email;
	      String pattern = "[\\w-]+@([\\w-]+\\.)+[a-zA-z]{2,}";

	      Pattern r = Pattern.compile(pattern);

	      Matcher m = r.matcher(line);
	      if (m.find( ) ) {
	         return true;
	      }

		return false;
	}
}
