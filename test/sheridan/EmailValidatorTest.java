package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsValidFormatRegular() {
		assertTrue("Invalid email format", EmailValidator.isValidFormat("chenhung@sheridancollege.ca"));
	}
	@Test
	public void testIsValidFormatException() {
		assertFalse("Invalid email format", EmailValidator.isValidFormat(null));
	}
	@Test
	public void testIsValidFormatBoundaryIn() {
		assertTrue("Invalid email format", EmailValidator.isValidFormat("a@b.c"));
	}
	@Test
	public void testIsValidFormatBoundaryOut() {
		assertFalse("Invalid email format", EmailValidator.isValidFormat("a@b. "));
	}
	
	@Test
	public void testIsValidAccountNameRegular() {
		assertTrue("Invalid email account name", EmailValidator.isValidAccountName("chen123@sheridancollege.ca"));
	}
	@Test
	public void testIsValidAccountNameException() {
		assertFalse("Invalid email account name", EmailValidator.isValidAccountName("1abc.@sheridancollege.ca"));
	}
	@Test
	public void testIsValidAccountNameBoundaryIn() {
		assertTrue("Invalid email account name", EmailValidator.isValidAccountName("abc00@b.c"));
	}
	@Test
	public void testIsValidAccountNameBoundaryOut() {
		assertFalse("Invalid email account name", EmailValidator.isValidAccountName("ab1@b.c"));
	}
@Test
	public void testIsValidDomainNameRegular() {
		assertTrue("Invalid email account name", EmailValidator.isValidDomainName("chen123@sh1233.com"));
	}
	@Test
	public void testIsValidDomainNameException() {
		assertFalse("Invalid email account name", EmailValidator.isValidDomainName("nancy@11.ca"));
	}
	@Test
	public void testIsValidDomainNameBoundaryIn() {
		assertTrue("Invalid email account name", EmailValidator.isValidDomainName("chenhung@ab1.com"));
	}
	@Test
	public void testIsValidDomainNameBoundaryOut() {
		assertFalse("Invalid email account name", EmailValidator.isValidDomainName("chen@a1.com"));
	}
	
	@Test
	public void testIsValidExtensionNameRegular() {
		assertTrue("Invalid email extension name", EmailValidator.isValidExtensionName("chen123@sh1233.com"));
	}
	@Test
	public void testIsValidExtensionNameException() {
		assertFalse("Invalid email extension name", EmailValidator.isValidExtensionName("nancy@11.c"));
	}
	@Test
	public void testIsValidExtensionNameBoundaryIn() {
		assertTrue("Invalid email extension name", EmailValidator.isValidExtensionName("chenhung@ab1.Aa"));
	}
	@Test
	public void testIsValidExtensionNameBoundaryOut() {
		assertFalse("Invalid email extension name", EmailValidator.isValidExtensionName("chen@a1.A1"));
	}


}
